#!/usr/bin/env python3

import argparse
import gitlab
from datetime import datetime
import csv
import os
import re

def read_allowed_list(filepath):
    allowed_users = set()
    with open(filepath,"r") as allowed_list:
        for line in allowed_list.readlines():
            allowed_users.add(line.lower().strip())
    return allowed_users

parser = argparse.ArgumentParser(description='Block/Delete inactive users')
parser.add_argument('gitlaburl', help='URL of the GitLab instance')
parser.add_argument('token', help='Admin API token to read the requested users')
parser.add_argument('inactivity_period', help='Inactivity period in days after users are blocked', type=int)
parser.add_argument('--no_dryrun', help='Execute blocking', action='store_true')
parser.add_argument('--allowed-list', help='List of accounts that will not be blocked, one username per line.')
parser.add_argument('--action_delete', help='Delete instead of block the user. This action only deletes the user, but maintains their contributions.', action='store_true')
parser.set_defaults(dryrun=True)
args = parser.parse_args()

gitlaburl = args.gitlaburl if args.gitlaburl.endswith("/") else args.gitlaburl + "/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

allowed_list_file = args.allowed_list
allowed_users = set()
if allowed_list_file:
    allowed_users = read_allowed_list(allowed_list_file)

# don't try to block internal users
allowed_users.add("support-bot")
allowed_users.add("alert-bot")
allowed_users.add("ghost")
allowed_users.add("root")

if not args.no_dryrun:
    print("This is a dry run, no user accounts will be blocked/delte. Run with --no-dryrun to execute blocking/deleting.")

try:
    users = gl.users.list(as_list=False)
except Exception as e:
    print("Can not retrieve list of users: "+ str(e))
    exit(1)

tz = datetime.now().astimezone().tzinfo

is_bot = lambda account: re.match('project_.*_bot', account.username)
nonbot_accounts = list(filter(lambda account: not is_bot(account), users))

period = args.inactivity_period

users_found = []

for user in nonbot_accounts:
    if user.attributes["state"] != "active":
        continue
    if "current_sign_in_at" not in user.attributes:
        print("Cannot access user field 'current_sign_in_at', use an admin token.")
        exit(1)
    else:
        if ( user.attributes["username"] not in allowed_users ):
            current_sign_in = user.attributes["current_sign_in_at"]
            last_activity = user.attributes["last_activity_on"]
            block = True
            if current_sign_in is not None:
                current_sign_in_dt = datetime.strptime(current_sign_in, '%Y-%m-%dT%H:%M:%S.%f%z')
                delta_sign_in = datetime.now(tz) - current_sign_in_dt
                if delta_sign_in.days < period:
                    block = False
            if last_activity is not None:
                last_activity_dt = datetime.strptime(last_activity, '%Y-%m-%d')
                delta_activity = datetime.now() - last_activity_dt
                if delta_activity.days < period:
                    block = False
            if block:
                if args.no_dryrun:
                    if not args.action_delete:
                        try:
                            print("Blocking %s" % user.attributes["username"])
                            user.block()
                            users_found.append(user.attributes)
                        except Exception as e:
                            print("Could not block %s: %s" % (user.attributes["username"], str(e)))
                    else:
                        try:
                            print("Deleting %s" % user.attributes["username"])
                            user.delete()
                            users_found.append(user.attributes)
                        except Exception as e:
                            print("Could not delete %s: %s" % (user.attributes["username"], str(e)))
                else:
                    users_found.append(user.attributes)

if args.no_dryrun:
    if args.action_delete:
        print("Deleted %s users, writing report." % str(len(users_found)))
        reportfilepath = "report/deleted_users_%s.csv" % str(datetime.now().date())
    else:
        print("Blocked %s users, writing report." % str(len(users_found)))
        reportfilepath = "report/blocked_users_%s.csv" % str(datetime.now().date())
else:
    if args.action_delete:
        print("There are %s users to delete, writing report." % str(len(users_found)))
        reportfilepath = "report/users_to_be_deleted_%s.csv" % str(datetime.now().date())
    else:
        print("There are %s users to delete, writing report." % str(len(users_found)))
        reportfilepath = "report/users_to_be_blocked_%s.csv" % str(datetime.now().date())

        

if not args.no_dryrun:
    reportfilepath = "report/users_to_be_blocked_%s.csv" % str(datetime.now().date())
if args.action_delete:
    reportfilepath = "report/users_to_be_deleted_%s.csv" % str(datetime.now().date())
os.makedirs(os.path.dirname(reportfilepath), exist_ok=True)

with open(reportfilepath, "w") as reportfile:
    reportwriter = csv.writer(reportfile, delimiter="\t", quotechar='"', quoting=csv.QUOTE_MINIMAL)
    fields = ["username","name","email","current_sign_in_at","last_activity_on"]
    reportwriter.writerow(fields)
    for user in users_found:
        row = []
        for field in fields:
            row.append(user[field])
        reportwriter.writerow(row)

